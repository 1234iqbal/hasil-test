<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidates', function (Blueprint $table) {
            $table->id('id');
            $table->string('name');
            $table->string('education');
            $table->date('birthday');
            $table->string('experience');
            $table->string('last_position');
            $table->string('applied_position');
            $table->string('skill');
            $table->string('email')->unique();
            $table->string('phone');
            $table->string('resume')->nullable();
            $table->bigInteger('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('candidates');
    }
};
