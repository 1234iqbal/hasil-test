<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::create([
            'name' => 'View Candidate',
            'guard_name' => 'web'
        ]);

        Permission::create([
            'name' => 'Read Candidate',
            'guard_name' => 'web'
        ]);

        Permission::create([
            'name' => 'Edit Candidate',
            'guard_name' => 'web'
        ]);

        Permission::create([
            'name' => 'Add Candidate',
            'guard_name' => 'web'
        ]);

        Permission::create([
            'name' => 'Delete Candidate',
            'guard_name' => 'web'
        ]);
    }
}
