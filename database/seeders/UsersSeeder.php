<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'Mr. John',
            'email' => 'jhon@gmail.com',
            'password' => Hash::make('secret1234')
        ]);
        $user->assignRole('Senior HRD');

        $user = User::create([
            'name' => 'Mrs. Lee',
            'email' => 'lee@gmail.com',
            'password' => Hash::make('secret1234')
        ]);
        $user->assignRole('HRD');
    }
}
