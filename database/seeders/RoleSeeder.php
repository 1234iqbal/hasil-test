<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::create([
            'name' => 'Senior HRD',
            'guard_name' => 'web'
        ]);
        $role->givePermissionTo(['View Candidate', 'Read Candidate', 'Edit Candidate', 'Add Candidate', 'Delete Candidate']);

        $role = Role::create([
            'name' => 'HRD',
            'guard_name' => 'web'
        ]);
        $role->givePermissionTo(['View Candidate', 'Read Candidate']);
    }
}
