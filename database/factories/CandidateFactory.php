<?php

namespace Database\Factories;

use App\Models\Candidate;
use Illuminate\Database\Eloquent\Factories\Factory;


class CandidateFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Candidate::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        
        return [
            'name' => $this->faker->text($this->faker->numberBetween(5, 15)),
            'education' => $this->faker->text($this->faker->numberBetween(5, 35)),
            'birthday' => $this->faker->date('Y-m-d'),
            'experience' => $this->faker->text($this->faker->numberBetween(5, 25)),
            'last_position' => $this->faker->text($this->faker->numberBetween(5, 10)),
            'applied_position' => $this->faker->text($this->faker->numberBetween(5, 10)),
            'skill' => $this->faker->text($this->faker->numberBetween(5, 25)),
            'email' => fake()->unique()->safeEmail(),
            'phone' => $this->faker->phoneNumber,
            'resume' => $this->faker->text($this->faker->numberBetween(5, 255)),
            'created_at' => $this->faker->date('Y-m-d H:i:s'),
            'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
