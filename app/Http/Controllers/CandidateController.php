<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCandidateRequest;
use App\Http\Requests\UpdateCandidateRequest;
use App\Http\Controllers\AppBaseController;
use App\Repositories\CandidateRepository;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Flash;
use Auth;

class CandidateController extends AppBaseController
{
    /** @var CandidateRepository $candidateRepository*/
    private $candidateRepository;

    public function __construct(CandidateRepository $candidateRepo)
    {
        $this->candidateRepository = $candidateRepo;
        $this->middleware('permission:View Candidate', ['only' => ['index']]);
        $this->middleware('permission:Add Candidate', ['only' => ['create', 'store']]);
        $this->middleware('permission:Read Candidate', ['only' => ['show']]);
        $this->middleware('permission:Edit Candidate', ['only' => ['edit', 'update']]);
        $this->middleware('permission:Delete Candidate', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the Batch.
     *
     * @param BatchDataTable $batchDataTable
     * @return Response
     */
    public function json()
    {
        $candidates = $this->candidateRepository->makeModel()->get();
        
        return Datatables::of($candidates)
            ->addColumn(
                'action',
                function ($data) {
                    $html = '<div class="btn-group">';

                    if ( Auth::user()->can('Read Candidate') ) {
                        $html = $html.'<a href="/candidates/'. $data->id.'"'.
                                    '<button class="btn btn-xs btn-primary">'.
                                        '<i class="icon-eye"></i>'.
                                    '</button>'.
                                '</a>';
                    }

                    if ( Auth::user()->can('Edit Candidate') ) {
                        $html = $html.'<a href="/candidates/'. $data->id.'/edit"'.
                                    '<button class="btn btn-xs btn-warning">'.
                                       '<i class="icon-note"></i>'.
                                    '</button>'.
                                '</a>';
                    }

                    if ( Auth::user()->can('Delete Candidate') ) {
                        $html = $html.'<button class="btn btn-xs btn-danger btn-delete" data-id="'.$data->id.'">'.
                                    '<i class="icon-trash"></i>'.
                                '</button>';
                    }

                    return $html.'</div>';
                }
            )
            ->addIndexColumn()
            ->make(true);
    }

    /**
     * Display a listing of the Candidate.
     */
    public function index(Request $request)
    {   
        $candidates = $this->candidateRepository->paginate(10);

        return view('candidates.index')
            ->with('candidates', $candidates);
    }

    /**
     * Show the form for creating a new Candidate.
     */
    public function create()
    {
        return view('candidates.create');
    }

    /**
     * Store a newly created Candidate in storage.
     */
    public function store(CreateCandidateRequest $request)
    {
        $input = $request->all();

        if ( $request->file('resume') ) {
            $file_name  = $request->file('resume')->getClientOriginalName();
            $file_name_new    = 'resume/'.\Carbon\Carbon::now()->format('Y-m-d h:i:s')."-".$file_name;

            Storage::disk('public')->put($file_name_new, file_get_contents($request->file('resume')) );
            $input['resume'] = $file_name_new;
        }

        if ( $input['birthday'] ) {
            $input['birthday'] = date('Y-m-d',strtotime($input['birthday']));
        }

        $candidate = $this->candidateRepository->create($input);

        Flash::success('Candidate saved successfully.');

        return redirect(route('candidates.index'));
    }

    /**
     * Display the specified Candidate.
     */
    public function show($id)
    {
        $candidate = $this->candidateRepository->find($id);

        if (empty($candidate)) {
            Flash::error('Candidate not found');

            return redirect(route('candidates.index'));
        }

        return view('candidates.show')->with('candidate', $candidate);
    }

    /**
     * Show the form for editing the specified Candidate.
     */
    public function edit($id)
    {
        $candidate = $this->candidateRepository->find($id);

        if (empty($candidate)) {
            Flash::error('Candidate not found');

            return redirect(route('candidates.index'));
        }

        return view('candidates.edit')->with('candidate', $candidate);
    }

    /**
     * Update the specified Candidate in storage.
     */
    public function update($id, UpdateCandidateRequest $request)
    {
        $candidate = $this->candidateRepository->find($id);

        if (empty($candidate)) {
            Flash::error('Candidate not found');

            return redirect(route('candidates.index'));
        }

        $input = $request->all();

        if ( $request->file('resume') ) {
            $file_name  = $request->file('resume')->getClientOriginalName();
            $file_name_new    = 'resume/'.\Carbon\Carbon::now()->format('Y-m-d h:i:s')."-".$file_name;

            Storage::disk('public')->put($file_name_new, file_get_contents($request->file('resume')) );
            $input['resume'] = $file_name_new;
        } else {
            $input['resume'] = $candidate->resume;
        }

        if ( $input['birthday'] ) {
            $input['birthday'] = date('Y-m-d',strtotime($input['birthday']));
        }

        $candidate = $this->candidateRepository->update($input, $id);

        Flash::success('Candidate updated successfully.');

        return redirect(route('candidates.index'));
    }

    /**
     * Remove the specified Candidate from storage.
     *
     * @throws \Exception
     */
    public function destroy($id)
    {
        $candidate = $this->candidateRepository->find($id);

        if (empty($candidate)) {
            return response()->json([
                'title'     => 'Candidate not found.',
                'text'      => '',
                'icon'      => 'error'
            ]);
            // Flash::error('Candidate not found');
            // return redirect(route('candidates.index'));
        }

        $this->candidateRepository->delete($id);
        // Flash::success('Candidate deleted successfully.');
        // return redirect(route('candidates.index'));
        return response()->json([
            'title'     => 'Candidate deleted successfully.',
            'text'      => '',
            'icon'      => 'success'
        ]);
    }
}
