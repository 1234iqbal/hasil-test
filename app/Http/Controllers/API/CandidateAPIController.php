<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCandidateAPIRequest;
use App\Http\Requests\API\UpdateCandidateAPIRequest;
use App\Models\Candidate;
use App\Repositories\CandidateRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Storage;

/**
 * Class CandidateController
 */

class CandidateAPIController extends AppBaseController
{
    private CandidateRepository $candidateRepository;

    public function __construct(CandidateRepository $candidateRepo)
    {
        $this->candidateRepository = $candidateRepo;
        $this->middleware('role:Senior HRD', ['only' => ['index', 'show']]);
        $this->middleware('role:Senior HRD', ['only' => ['store', 'update', 'destroy']]);
        
    }

    public function index(Request $request): JsonResponse
    {
        $candidates = $this->candidateRepository->all();

        return $this->sendResponse($candidates->toArray(), 'Candidates retrieved successfully');
    }

    public function store(CreateCandidateAPIRequest $request): JsonResponse
    {
        $input = $request->all();

        if ( $request->file('resume') ) {
            $file_name  = $request->file('resume')->getClientOriginalName();
            $file_name_new    = 'resume/'.\Carbon\Carbon::now()->format('Y-m-d h:i:s')."-".$file_name;

            Storage::disk('public')->put($file_name_new, file_get_contents($request->file('resume')) );
            $input['resume'] = $file_name_new;
        }

        if ( $input['birthday'] ) {
            $input['birthday'] = date('Y-m-d',strtotime($input['birthday']));
        }

        $candidate = $this->candidateRepository->create($input);

        return $this->sendResponse($candidate->toArray(), 'Candidate saved successfully');
    }

    public function show($id): JsonResponse
    {
        /** @var Candidate $candidate */
        $candidate = $this->candidateRepository->find($id);

        if (empty($candidate)) {
            return $this->sendError('Candidate not found');
        }

        return $this->sendResponse($candidate->toArray(), 'Candidate retrieved successfully');
    }

    public function update($id, UpdateCandidateAPIRequest $request): JsonResponse
    {
        $input = $request->all();

        /** @var Candidate $candidate */
        $candidate = $this->candidateRepository->find($id);

        if (empty($candidate)) {
            return $this->sendError('Candidate not found');
        }

        if ( $request->file('resume') ) {
            $file_name  = $request->file('resume')->getClientOriginalName();
            $file_name_new    = 'resume/'.\Carbon\Carbon::now()->format('Y-m-d h:i:s')."-".$file_name;

            Storage::disk('public')->put($file_name_new, file_get_contents($request->file('resume')) );
            $input['resume'] = $file_name_new;
        } else {
            $input['resume'] = $candidate->resume;
        }

        if ( $input['birthday'] ) {
            $input['birthday'] = date('Y-m-d',strtotime($input['birthday']));
        }

        $candidate = $this->candidateRepository->update($input, $id);

        return $this->sendResponse($candidate->toArray(), 'Candidate updated successfully');
    }

    public function destroy($id): JsonResponse
    {
        /** @var Candidate $candidate */
        $candidate = $this->candidateRepository->find($id);

        if (empty($candidate)) {
            return $this->sendError('Candidate not found');
        }

        $candidate->delete();

        return $this->sendSuccess('Candidate deleted successfully');
    }
}
