<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\LoginAPIRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Models\User;
use Hash;


class AuthenticationController extends AppBaseController
{
    public function login(LoginAPIRequest $request) {
        try {
            $user = User::where('email', $request->email)->first(); 

            if ( $user ) {
                if( Hash::check( $request->password, $user->password ) ){
                    $token = $user->createToken('nApp')->accessToken;
                    
                    return $this->sendResponse([
                        "token" => $token,
                        "user" => $user
                    ], 'You are successfully logged in');

                } else {
                    return $this->sendError('password tidak cocok', 422);
                }
            } else {
                return $this->sendError('email belum terdaftar', 401);
            }
        } catch (\Exception $e) {
            return $this->sendError( $e->getMessage() );
        }
        
    }

    public function logout(Request $request) {
        try {
            $response = $request->user('api')->token()->revoke();

            return $this->sendResponse([], 'You have successfully logged out');
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(), 400);
        }
    }
}
