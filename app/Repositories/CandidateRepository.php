<?php

namespace App\Repositories;

use App\Models\Candidate;
use App\Repositories\BaseRepository;

class CandidateRepository extends BaseRepository
{
    protected $fieldSearchable = [
        'name',
        'education',
        'birthday',
        'experience',
        'last_position',
        'applied_position',
        'skill',
        'email',
        'phone',
        'resume'
    ];

    public function getFieldsSearchable(): array
    {
        return $this->fieldSearchable;
    }

    public function model(): string
    {
        return Candidate::class;
    }
}
