<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Candidate extends Model
{
    use HasFactory;
    
    public $table = 'candidates';

    public $fillable = [
        'name',
        'education',
        'birthday',
        'experience',
        'last_position',
        'applied_position',
        'skill',
        'email',
        'phone',
        'resume'
    ];

    protected $casts = [
        'name' => 'string',
        'education' => 'string',
        'birthday' => 'date:Y-m-d',
        'experience' => 'string',
        'last_position' => 'string',
        'applied_position' => 'string',
        'skill' => 'string',
        'email' => 'string',
        'phone' => 'string',
        'resume' => 'string'
    ];

    public static $rules = [
        'name' => 'required',
        'education' => 'required',
        'birthday' => 'required',
        'experience' => 'required',
        'last_position' => 'required',
        'applied_position' => 'required',
        'skill' => 'required',
        'email' => 'required|string|email|max:255',
        'phone' => 'required|max:18|min:10',
    ];

    
}
