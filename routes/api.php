<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\API\AuthenticationController;
use App\Http\Controllers\API\CandidateAPIController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', [AuthenticationController::class, 'login']);

Route::group([ 'middleware' => ['auth:api']], function() {
    Route::get('/logout', [ AuthenticationController::class, 'logout']);

    // Candidate
    Route::group([ 'prefix' =>'/candidates'], function() {
        Route::get('/', [ CandidateAPIController::class, 'index' ]);
        Route::post('/', [ CandidateAPIController::class, 'store' ]);
        Route::get('/{id}', [ CandidateAPIController::class, 'show' ]);
        Route::post('/{id}', [ CandidateAPIController::class, 'update' ]);
        Route::delete('/{id}', [ CandidateAPIController::class, 'destroy' ]);
    });
});