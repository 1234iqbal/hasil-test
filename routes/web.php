<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\CandidateController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return redirect('login'); });
Auth::routes(['register' => false]);

// Dashboard
Route::group([ 'middleware' =>'auth:sanctum'], function(){

    // Dashboard
    Route::get('/dashboard', [ DashboardController::class, 'index']);

    // Candidate
    Route::group([ 'prefix' =>'/candidates' , 'as' => 'candidates.'], function() {
        Route::get('/json', [ CandidateController::class, 'json'])->name('json');
        Route::get('/', [ CandidateController::class, 'index'])->name('index');
        Route::post('/', [ CandidateController::class, 'store'])->name('store');
        Route::get('/create', [ CandidateController::class, 'create'])->name('create');
        Route::get('/{id}', [ CandidateController::class, 'show'])->name('show');
        Route::patch('/{id}', [ CandidateController::class, 'update'])->name('update');
        Route::delete('/{id}', [ CandidateController::class, 'destroy'])->name('destroy');
        Route::get('/{id}/edit', [ CandidateController::class, 'edit'])->name('edit');
    });
});

