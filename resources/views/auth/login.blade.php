@extends('layouts.authentication')

@section('content')
    <div class="wrapper wrapper-login">
        <div class="container container-login animated fadeIn">
            <h3 class="text-center">Login Account</h3>
            <form method="POST" action="{{ route('login') }}">
            @csrf
                <div class="login-form">
                    <div class="form-group form-floating-label @error('email') has-error @enderror">
                        <input id="email" type="email" name="email" value="{{ old('email') }}" class="form-control input-border-bottom" required autofocus>
                        <label for="email" class="placeholder">Email</label>
                        @error('email')
                            <span class="form-text" style="color:red;">{{ $message }}</span>
                        @enderror
                    </div>
                    
                    <div class="form-group form-floating-label">
                        <input id="password" type="password" name="password" required autocomplete="current-password" class="form-control input-border-bottom">
                        <label for="password" class="placeholder">Password</label>
                        <div class="show-password">
                            <i class="icon-eye"></i>
                        </div>
                        @error('password')
                            <span class="form-text" style="color:red;">{{ $message }}</span>
                        @enderror
                    </div>
                    

                    
                    <div class="form-action mb-3">
                        <button type="submit" class="btn btn-primary btn-rounded btn-login">Login</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection