
@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="panel-header bg-primary-gradient">
            <div class="page-inner py-5">
                <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                    <div>
                        <h2 class="text-white pb-2 fw-bold">Update Candidate</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="page-inner mt--5">
            <div class="row mt--2">
                <div class="col-md-12">
                    <div class="card">
                        @include('adminlte-templates::common.errors')
                        <div class="card-body">
                            {!! Form::model($candidate, ['route' => ['candidates.update', $candidate->id], 'method' => 'patch',  'enctype' => 'multipart/form-data']) !!}

                                @include('candidates.fields')

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('page_js')
    <script>
        // Set value date
        $('input[name="birthday"]').val("{{ date('d-m-Y', strtotime($candidate->birthday)) }}");
    </script>
@endpush