@section('page_css')
    @include('layouts.plugins.datatables.datatables_css')
@endsection

<table id="table-candidate" class="display table table-striped table-hover" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Action</th>
        </tr>
    </thead>
</table>

@push('page_js')

    @include('layouts.plugins.datatables.datatables_js')

    <script>
        function refresh_data() {
            $('#table-candidate').DataTable({
                processing: true,
                serverSide: true,
                destroy: true,
                ajax: {
                    url: "{{ url('/candidates/json') }}",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    method: 'GET',
                },
                columns: [
                    { data: 'name' },
                    { data: 'email' },
                    { data: 'phone' },
                    {
                        data: 'action',
                        searchable: false,
                        class: 'text-center',
                        width: '20px'
                    },
                ]
            })
        }

        $(document).ready(function() {
            refresh_data();
        });
    </script>

    <script>
        $(document).on('click', '.btn-delete', function() {
            swal({
                title: 'Are you sure?',
                text: '',
                icon: 'warning',
                buttons: {
                    cancel: {
                        visible: true,
                        className: 'btn btn-default'
                    },
                    confirm: {
                        text: 'Hapus',
                        className: 'btn btn-danger'
                    },
                }
            }).then((Confirm) => {
                if (Confirm) {
                    $.ajax({
                        url: "{{ url('candidates') }}" + '/' + $(this).data('id'),
                        type: 'DELETE',
                        dataType: "JSON",
                        data : { 
                            _token: '{{csrf_token()}}', 
                        },
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(data) {
                            swal({
                                title: data.title,
                                text: data.text,
                                icon: data.icon
                            }).then((Confirm) => {
                                window.location.reload()
                            })
                        }
                    })
                }
            })
        });
    </script>

@endpush