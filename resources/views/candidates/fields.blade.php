<div class="row">
    <!-- Name Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('name', 'Name :') !!}
        {!! Form::text('name', null, ['class' => 'form-control', 'required', 'placeholder' => 'smith']) !!}
    </div>

    <!-- Email Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('email', 'Email :') !!}
        {!! Form::email('email', null, ['class' => 'form-control', 'required', 'placeholder' => 'smith@gmail.com']) !!}
    </div>
</div>

<div class="row">
    <!-- Phone Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('phone', 'Phone :') !!}
        {!! Form::number('phone', null, ['class' => 'form-control', 'required', 'placeholder' => '085123456789']) !!}
    </div>

    <!-- Birthday Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('birthday', 'Date Of Birth :') !!}
        <div class="input-group">
            <input type="text" class="form-control" name="birthday" required placeholder="23-10-2022" autocomplete="off" required>
            <div class="input-group-append">
                <span class="input-group-text">
                    <i class="fa fa-calendar"></i>
                </span>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <!-- Education Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('education', 'Education :') !!}
        {!! Form::text('education', null, ['class' => 'form-control', 'required', 'placeholder' => 'UGM Yogyakarta']) !!}
    </div>

    <!-- Experience Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('experience', 'Experience :') !!}
        {!! Form::text('experience', null, ['class' => 'form-control', 'required', 'placeholder' => '5 Year']) !!}
    </div>
</div>

<div class="row">
    <!-- Last Position Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('last_position', 'Last Position :') !!}
        {!! Form::text('last_position', null, ['class' => 'form-control', 'required', 'placeholder' => 'CEO']) !!}
    </div>

    <!-- Applied Position Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('applied_position', 'Applied Position :') !!}
        {!! Form::text('applied_position', null, ['class' => 'form-control', 'required', 'placeholder' => 'Senior PHP Developer']) !!}
    </div>
</div>

<div class="row">
    <!-- Resume Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('resume', 'Upload resume :') !!}
        {!! Form::file('resume', ['id' => 'resume', 'class' => "form-control-file", 'accept' => "application/pdf" ]) !!}
    </div>

    <!-- Skill Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('skill', 'Skill :') !!}
        {!! Form::text('skill', null, ['class' => 'form-control', 'required', 'placeholder' => 'Laravel, Mysql, PostgreSQL, Codeigniter, Java']) !!}
    </div>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('candidates.index') }}" class="btn btn-default">Cancel</a>
</div>


@push('page_js')

    @include('layouts.plugins.momment.momment_js')
    @include('layouts.plugins.date_picker.date_picker_js')

    <script>
        $('input[name="birthday"]').datetimepicker({
            format: 'DD-MM-YYYY',
        });
    </script>


@endpush