@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="panel-header bg-primary-gradient">
            <div class="page-inner py-5">
                <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                    <a href="{{ route('candidates.index') }}" class="btn btn-info btn-round ">
                        <i class="fas fa-arrow-left"></i> Back
                    </a>
                </div>
            </div>
        </div>
        
        <div class="page-inner mt--5">
            <div class="row mt--2">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            @include('candidates.show_fields')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
