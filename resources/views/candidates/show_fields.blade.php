<!-- Name Field -->
<div class="col-sm-12">
    {!! Form::label('name', 'Name. ex :', ['style'=>'font-weight: bold;']) !!}
    <p>{{ $candidate->name }}</p>
</div>

<!-- Education Field -->
<div class="col-sm-12">
    {!! Form::label('education', 'Education. ex :', ['style'=>'font-weight: bold;']) !!}
    <p>{{ $candidate->education }}</p>
</div>

<!-- Birthday Field -->
<div class="col-sm-12">
    {!! Form::label('birthday', 'Birthday. ex :', ['style'=>'font-weight: bold;']) !!}
    <p>{{ date('d-m-Y',strtotime($candidate->birthday))  }}</p>
</div>

<!-- Experience Field -->
<div class="col-sm-12">
    {!! Form::label('experience', 'Experience. ex :', ['style'=>'font-weight: bold;']) !!}
    <p>{{ $candidate->experience }}</p>
</div>

<!-- Last Position Field -->
<div class="col-sm-12">
    {!! Form::label('last_position', 'Last Position. ex :', ['style'=>'font-weight: bold;']) !!}
    <p>{{ $candidate->last_position }}</p>
</div>

<!-- Applied Position Field -->
<div class="col-sm-12">
    {!! Form::label('applied_position', 'Applied Position. ex :', ['style'=>'font-weight: bold;']) !!}
    <p>{{ $candidate->applied_position }}</p>
</div>

<!-- Skill Field -->
<div class="col-sm-12">
    {!! Form::label('skill', 'Top 5 Skills. ex :', ['style'=>'font-weight: bold;']) !!}
    <p>{{ $candidate->skill }}</p>
</div>

<!-- Email Field -->
<div class="col-sm-12">
    {!! Form::label('email', 'Email. ex :', ['style'=>'font-weight: bold;']) !!}
    <p>{{ $candidate->email }}</p>
</div>

<!-- Phone Field -->
<div class="col-sm-12">
    {!! Form::label('phone', 'Phone. ex :', ['style'=>'font-weight: bold;']) !!}
    <p>{{ $candidate->phone }}</p>
</div>

<!-- Resume Field -->
<div class="col-sm-12">
    {!! Form::label('resume', 'Resume. ex :', ['style'=>'font-weight: bold;']) !!}
    <p>
        <a href="{{ Storage::url($candidate->resume) }}" target="_blank">{{ str_replace("resume/","", $candidate->resume) }}</a>
    </p>
</div>

