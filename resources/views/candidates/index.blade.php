@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="panel-header bg-primary-gradient">
            <div class="page-inner py-5">
                <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                    <div>
                        <h2 class="text-white pb-2 fw-bold">Candidate</h2>
                    </div>
                    @can('Add Candidate')
                        <div class="ml-md-auto py-2 py-md-0">
                            <a href="{{ url('/candidates/create') }}" class="btn btn-secondary btn-round">Add</a>
                        </div>
                    @endcan
                </div>
            </div>
        </div>
        <div class="page-inner mt--5">
            <div class="row mt--2">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                @include('candidates.table')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('page_js')
    <!-- Datatables -->
    <script src="{{ asset('assets/js/plugin/datatables/datatables.min.js')}}"></script>
    <script>
        // Add Row
        $('#add-row').DataTable();
    </script>
@endpush