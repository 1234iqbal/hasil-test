<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from themekita.com/demo-atlantis-bootstrap/livepreview/examples/demo1/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 30 Oct 2019 02:01:29 GMT -->
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
	<title>Test Project</title>
	<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('img/apple-touch-icon.png') }}">
	<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('img/favicon-32x32.png') }}">
	<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('img/favicon-16x16.png') }}">
	<!-- <link rel="manifest" href="{{ asset('img/site.webmanifest') }}"> -->
    
    @include('layouts.template.style')
    @stack('page_css')
</head>
<body>
	<div class="wrapper">
		<div class="main-header">
			<!-- Logo Header -->
			@include('layouts.template.header')
			<!-- End Logo Header -->

			<!-- Navbar Header -->
			@include('layouts.template.navbar')
			<!-- End Navbar -->
		</div>

		<!-- Sidebar -->
		@include('layouts.template.sidebar')
		<!-- End Sidebar -->

		<div class="main-panel">
			@yield('content')
			@include('layouts.template.footer')
		</div>
		
	</div>
    @include('layouts.template.script')
    @stack('page_js')
</body>

</html>