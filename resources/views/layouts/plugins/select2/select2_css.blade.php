<style>
    /*     Label states for select2      */
    .select2-input {
    position: relative; }
    .select2-input label.error, .select2-input label.success {
        position: absolute;
        bottom: -30px; }
    .select2-input .select2 {
        margin-bottom: 15px; }
</style>