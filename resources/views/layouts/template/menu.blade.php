<li class="nav-item {{ Request::is('dashboard') ? 'active' : '' }}">
    <a href="{{ url('/dashboard') }}" class="collapsed" aria-expanded="false">
        <i class="fas fa-home"></i>
        <p>Dashboard</p>
    </a>
</li>

<li class="nav-item {{ Request::is('candidates*') ? 'active' : '' }}">
    <a href="{{ url('/candidates') }}" class="collapsed" aria-expanded="false">
        <i class="fas fa-user"></i>
        <p>Candidates</p>
    </a>
</li>