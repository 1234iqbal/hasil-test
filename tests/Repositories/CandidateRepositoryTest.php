<?php

namespace Tests\Repositories;

use App\Models\Candidate;
use App\Repositories\CandidateRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class CandidateRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    protected CandidateRepository $candidateRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->candidateRepo = app(CandidateRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_candidate()
    {
        $candidate = Candidate::factory()->make()->toArray();

        $createdCandidate = $this->candidateRepo->create($candidate);

        $createdCandidate = $createdCandidate->toArray();
        $this->assertArrayHasKey('id', $createdCandidate);
        $this->assertNotNull($createdCandidate['id'], 'Created Candidate must have id specified');
        $this->assertNotNull(Candidate::find($createdCandidate['id']), 'Candidate with given id must be in DB');
        $this->assertModelData($candidate, $createdCandidate);
    }

    /**
     * @test read
     */
    public function test_read_candidate()
    {
        $candidate = Candidate::factory()->create();

        $dbCandidate = $this->candidateRepo->find($candidate->id);

        $dbCandidate = $dbCandidate->toArray();
        $this->assertModelData($candidate->toArray(), $dbCandidate);
    }

    /**
     * @test update
     */
    public function test_update_candidate()
    {
        $candidate = Candidate::factory()->create();
        $fakeCandidate = Candidate::factory()->make()->toArray();

        $updatedCandidate = $this->candidateRepo->update($fakeCandidate, $candidate->id);

        $this->assertModelData($fakeCandidate, $updatedCandidate->toArray());
        $dbCandidate = $this->candidateRepo->find($candidate->id);
        $this->assertModelData($fakeCandidate, $dbCandidate->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_candidate()
    {
        $candidate = Candidate::factory()->create();

        $resp = $this->candidateRepo->delete($candidate->id);

        $this->assertTrue($resp);
        $this->assertNull(Candidate::find($candidate->id), 'Candidate should not exist in DB');
    }
}
