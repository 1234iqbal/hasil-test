<?php

namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Candidate;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;

class CandidateApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_candidate()
    {
        Storage::fake('public/resume');
        $file = UploadedFile::fake()->create('gemar-membaca.pdf')->store('public/resume');
        $candidate = Candidate::factory()->make()->toArray();
        $candidate['resume'] = $file;
        $candidate['birthday'] = date('Y-m-d',strtotime($candidate['birthday']));

        $this->response = $this->json(
            'POST',
            '/api/v1/candidates', $candidate
        );

        $this->assertApiResponse($candidate);
    }

    /**
     * @test
     */
    public function test_read_candidate()
    {
        $candidate = Candidate::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/v1/candidates/'.$candidate->id
        );

        $this->assertApiResponse($candidate->toArray());
    }

    /**
     * @test
     */
    public function test_update_candidate()
    {
        Storage::fake('public/resume');
        $file = UploadedFile::fake()->create('gemar-membaca.pdf')->store('public/resume');
        $candidate = Candidate::factory()->create();
        $editedCandidate = Candidate::factory()->make()->toArray();
        $editedCandidate['resume'] = $file;
        $editedCandidate['birthday'] = date('Y-m-d',strtotime($editedCandidate['birthday']));

        $this->response = $this->json(
            'POST',
            '/api/v1/candidates/'.$candidate->id,
            $editedCandidate
        );
        
        $this->assertApiSuccess();
    }

    /**
     * @test
     */
    public function test_delete_candidate()
    {
        $candidate = Candidate::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/v1/candidates/'.$candidate->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/v1/candidates/'.$candidate->id
        );

        $this->response->assertStatus(404);
    }
}
